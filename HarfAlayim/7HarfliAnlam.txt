Futbolda topa olanca gücüyle vurmak
Kent içinde ulaşımı sağlayan otobüslerde para yerine geçen bilet veya kart
Avustralya yerlisi
Soğurma
Tez iş gören, çabuk davranan, canı tez, farfara, fırtına gibi, içi tez, ivecen, iveğen, kıvrak, sabırsız
Toyca, beceriksizce
Mala, yiyeceğe ve içeceğe doymak bilmeyen, açgöz, gözü aç, doymaz, gözü doymaz, tamahkâr
Uyanık davranarak çıkar sağlayan, imkânlardan kurnazca yararlanmasını bilen, cingöz
Bir aletin çapları birbirinden farklı olan parçalarından birini ötekine geçirebilmek için yararlanılan bağlayıcı
Hesap
Sağlıklı bir vücuda sahip olmak için tempolu müzik eşliğinde yapılan bir jimnastik türü
Saldırgan
Yer çekiminin, bir cismin molekülleri üzerindeki etkisinin oluşturduğu bileşke, ağır olma durumu
Üzüntü, acı, sevinç, pişmanlık vb.nin etkisiyle gözyaşı dökmek
Kafadan bacaklılardan, dokunaçlı bir tür mürekkep balığı
Yüksekokul
Göğüs kafesinin içinde yer alan, sağlı sollu iki parçadan oluşan solunum organı
Suçsuz veya borçsuz olduğu yargısına vararak birini temize çıkarmak, tebriye etmek, ibra etmek
Cambaz
Bir işin yolunda gitmemesi durumu, terslik
Bir kuvvetin, maddi bir etkenin, bir düşüncenin ortaya çıkması, hareket, iş
Bir yolcunun gideceği yere birkaç araç değiştirerek ulaşması
Yankı bilimi
Kan, lenf vb. vücut sıvılarında bulunan çekirdekli, yuvarlak hücre
Geminin yan yatması
Her zaman görülen, olağan, bayağı
Bayrağı veya sancağı taşıyan kimse, önder
Alerjiye sebep olan herhangi bir madde
Çabuk gücenen, kırılan
Avrupa'da genellikle de Almanya'da çalışan Türk vatandaşı
Bir yerleşim yeri veya bir yapı için gerekli olan yol, kanalizasyon, su, elektrik vb. tesisatın tümü
Kana al rengini veren, çekirdeksiz, yuvarlak, küçük hücre
Eşyayı sarmaya yarayan mukavva, kâğıt, tahta, plastik vb. malzeme
Bir devletin, gemilerin kendi limanlarından ayrılmasını yasaklama buyruğu, engelleyim
Süs taşı olarak kullanılan, mor renkte bir kuvars türü
Ön Asya'nın bir parçası olarak Türkiye'nin Asya kıtasında bulunan toprağı, Küçük Asya, Rum
Kilidi açıp kapamak için kullanılan araç
Hastanın, hastalığı ve çevresi hakkında verdiği bilgi
İnsan, hayvan ve bitkilerin yapısını ve organlarının birbiriyle olan ilgilerini inceleyen bilim
Bir devletin yönetim biçimini belirten, yasama, yürütme, yargılama güçlerinin nasıl kullanılacağını gösteren, yurttaşların kamu haklarını bildiren temel yasa
Bir kimseye veya bir topluluğa zorla, ücret vermeden yaptırılan iş, yüklenti
Genel olana, alışılmışa ve kurala aykırı olan, normal olmayan, düzgüsüz
Panzehir
Vücuda girişi kendisine karşı antikor oluşmasına sebep olan protein yapısında madde
Vücuda giren antijenlere karşı oluşan bağışıklık proteini
Boksta bükük kolla aşağıdan yukarıya doğru çeneye atılan yumruk
Arap müziğini andıran, genellikle karamsarlığı konu edinen bir müzik türü
Kundaklı, tetikli yay
Kanıt, tez, iddia, sav
Birbirlerine karşı sevgi ve anlayış gösteren kimselerden her biri, dost
Birini sevindirmek, mutlu etmek, onurlandırmak, kutlamak için veya anı olarak verilen şey
Avusturya'da imparator ailesi prenslerine verilen unvan
İnsanları, yükleri bir yapının bir katından ötekine veya yüksek yerlere çıkarıp indiren, elektrikle işleyen araç
Doğru olmayan, temelsiz, köksüz, dayanaksız, yalan (haber)
Yardımcı, araştırma görevlisi
Sosyal olmayan
Ağrı kesici, ateş düşürücü, kan sulandırıcı ve yangı giderici olarak kullanılan hap
Âşığa yaraşır bir biçimde olan
Açıkça, belli ederek, saklamadan
Soyda temel olarak babayı alan ve ailede çocukları baba soyuna mal eden topluluk düzeni
Uzun deneme ve gözlemlere dayanılarak söylenmiş ve halka mal olmuş, öğüt verici nitelikte söz
Savaşan iki kuvvetin karşılıklı olarak savaşı durdurması, bırakışma
Vücudu gelişmiş, biçimli
Üstünlük, kazanım, yarar
Avrupalılara özgü, Avrupalılara benzer
Bir şeyle uğraşarak acısını unutmak, sıkıntılardan uzaklaşmak
Bir toplulukta kendine özgü nitelikler bakımından ayrı ve ötekilerden sayıca az olanlar, azlık
İradeli, gayretli, istençli, kararlı
















