Su
Yemek yemesi gereken, tok karşıtı
İsim
Bir suçu, bir kusuru veya bir hatayı bağışlama
Örümcek vb. hayvanların salgılarıyla oluşturdukları örgü
Temiz, dürüst, beyaz
Kanın rengi
Zamanın bölünemeyecek kadar kısa olan parçası, lahza, dakika
Utanma, utanç duyma
Bir işte başta gelen (kimse veya şey)
Yemek
Satrançta, her yönde siyahtan beyaza ve beyazdan siyaha bir hane atlayarak L biçiminde hareket eden taş
Bir hayvanın bir başka hayvanı yemek için yakalaması
Dünya'nın uydusu olan gök cismi, kamer, mah, meh
Nicelik, nitelik, güç, süre, sayı bakımından eksik, çok karşıtı
Yerde, zamanda veya söz zincirinde en yakın olanı gösteren bir söz
Kucak çocuklarını, bebekleri eğlendirmek için çıkarılan ses
Olur, peki veya fena değil` anlamlarında kullanılan bir söz. Bezginlik anlatan bir söz 
Bir gazete veya derginin günlük yayımından ayrı ve ücretsiz olarak verdiği parça, ilave
Yakınların dışında kalan kimse, yabancı
Bir yüzeyde boy sayılan iki kenar arasındaki uzaklık, genişlik, boy, uzunluk karşıtı
Rütbesiz asker, nefer
Notada duraklama zamanı ve bunu gösteren işaretin adı
Karı kocadan her biri, hayat arkadaşı, refik, refika
İnsanlarda, hayvanlarda deri ile kemik arasındaki kas ve yağdan oluşan tabaka
Bir kimsenin veya ailenin içinde yaşadığı yer, konut, hane
Herhangi bir durumun, cismin veya alanın sınırları arasında bulunan bir yer, dâhil, dış karşıtı
Pamuk, iplik eğirmekte kullanılan, ortası şişkin, iki ucu sivri ve çengelli olan, ağaçtan yapılmış araç
Şehrin niteliklerini taşıyan büyük yerleşim yeri
Yaban hayvanlarının kendilerine yuva edindikleri kovuk
Dumanın değdiği yerde bıraktığı kara leke
Geçim sağlamak için herhangi bir alanda yapılan çalışma, meslek
Köpek
Bir şeyin geçtiği veya önce bulunduğu yerde bıraktığı belirti, nişan, alamet, emare
Öyle, o kadar, o denli` vb.nden sonra, kullanıldığı cümleye güç katan bir söz
Koyun, kuzu vb. hayvanların çıkardığı ses
Soru biçiminde şaşma bildiren ünlem
Ateş
Sıkıntı, bezginlik, usanç, acı, yorgunluk vb. duyguları belirten bir söz. Trabzon'un bir ili.
Yayla atılan, ucunda sivri bir demir bulunan ince ve kısa tahta çubuk
Toprak üstündeki bölümleri odunlaşmayıp yumuşak kalan, ilkbaharda bitip bir iki mevsim sonra kuruyan küçük bitkiler
Seçimlerde kişinin herhangi bir aday veya partiye ait yaptığı tercih
Kötü bir davranış veya sözü cezalandırmak için kötülükle karşılık verme isteği ve işi, intikam
Bir şeyin esas tutulan yüzü, arka karşıtı
Bir kimsenin benliği, kendi manevi varlığı, iç, nefis, derun, varoluş karşıtı
Bir sıvının asit veya bazlık derecesi, sertlik derecesi
Hidrojenle oksijenden oluşan, sıvı durumunda bulunan, renksiz, kokusuz, tatsız madde, ab
Kurşun kalemlerde yazmayı sağlayan kömürden yapılan madde. Dış kenar
Öğütülerek toz durumuna getirilmiş tahıl ve başka besin maddeleri
Hücrelerin aşırı çoğalmasıyla insan, hayvan veya bitki dokularında oluşan ve büyüme eğilimi gösteren yumru, tümör
Akıl
Klasik Türk müziği araçlarından, iri karınlı, kirişli, mızrapla çalınan bir çalgı
Herkesçe bilinme, tanınma durumu, san, şöhret, şan
Bir askerî harekâtta birliklerin gereksinim duyduğu her türlü gerecin toplandığı, dağıtıldığı bölge
İki kelime veya iki cümle arasına girerek aralarında bir bağ olduğunu anlatan söz