﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PanelBtnManager : MonoBehaviour
{
   public void StarButton()
    {
        SceneManager.LoadScene("GameScene");
    }
   public void QuitButton()
   {
       Application.Quit();
   } 
}
