﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    static bool firstClick = false;
    public AudioSource audioListener;
    string word;
    public void Clicked()
    {
        audioListener.Play();
        word += GetComponentInChildren<Text>().text;
        GameManager.truWord += word.ToLower();
        word = "";
        Debug.Log("kelime: " + GameManager.truWord);
    }
    public void MouseClick()
    {        
        if (firstClick == false && GetComponentInChildren<Text>().color != Color.white)
        {
            GetComponentInChildren<Text>().color = Color.white;
            firstClick = true;
            Clicked();
            audioListener.Play();
            Debug.Log("tıklandı");
        }
    }
    public void MouseEnter()
    {
        if (firstClick == true && GetComponentInChildren<Text>().color != Color.white)
        {
            Clicked();
            GetComponentInChildren<Text>().color = Color.white;
            Debug.Log("üzerine geldi");
        }
    }
    public void MouseUp()
    {
        if (GameManager.isTrue == false)
        {
            firstClick = false;
            Debug.Log("bıraktı");
            GameManager.truWord = "";
        }
        else
        {
            firstClick = false;
            Debug.Log("bıraktı");
        }
    }
    private void Update()
    {
        if (firstClick == false)
        {
            GetComponentInChildren<Text>().color = Color.black;
        }
    }
}
