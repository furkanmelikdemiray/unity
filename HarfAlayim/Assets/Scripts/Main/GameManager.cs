﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
public class GameManager : MonoBehaviour
{
    string[] kelimeler = new string[8];
    int num;
    public static int i = 0, harfLenght, puan = 0;
    public static bool isTrue = false,isGameOver=false;
    public Text SözlükText;
    public GameObject[] prefab = new GameObject[8];
    [SerializeField] private string sözlükAnlamı;
    [SerializeField] private bool dogruMu = false;
    [SerializeField] private GameObject circle;
    [SerializeField] private Text wordText;
    [SerializeField] private Text levelText;
    public TextAsset harfli2Asset;
    public TextAsset harfli2AnlamAsset;
    public TextAsset harfli3Asset;
    public TextAsset harfli3AnlamAsset;
    public TextAsset harfli4Asset;
    public TextAsset harfli4AnlamAsset;
    public TextAsset harfli5Asset;
    public TextAsset harfli5AnlamAsset;
    public TextAsset harfli6Asset;
    public TextAsset harfli6AnlamAsset;
    public TextAsset harfli7Asset;
    public TextAsset harfli7AnlamAsset;
    public TextAsset harfli8Asset;
    public TextAsset harfli8AnlamAsset;
    public TextAsset harfli8Asset2;
    public TextAsset harfli8AnlamAsset2;
    string[] harfli2 = new string[] { };
    string[] harfli2Anlam = new string[] { };
    string[] harfli3 = new string[] { };
    string[] harfli3Anlam = new string[] { };
    string[] harfli4 = new string[] { };
    string[] harfli4Anlam = new string[] { };
    string[] harfli5 = new string[] { };
    string[] harfli5Anlam = new string[] { };
    string[] harfli6 = new string[] { };
    string[] harfli6Anlam = new string[] { };
    string[] harfli7 = new string[] { };
    string[] harfli7Anlam = new string[] { };
    string[] harfli8 = new string[] { };
    string[] harfli8Anlam = new string[] { };
    string[] harfli82 = new string[] { };
    string[] harfli8Anlam2 = new string[] { };
    string harf, harf2;
    public static string truWord;
    string st = "ABCÇDEFGĞHİIJKLMNOÖPRSŞTUÜVYZ";
    char c;
    List<char> harfListe = new List<char>();
    List<int> numbers = new List<int>();
    private int lastNumber, levelNumber = 0;
    int tmp;
    [SerializeField] private static int random;
    public AudioSource audioSource;
    public AudioSource audioSource2;
    [SerializeField] private Text txt, puanTxt, harflAlmaHakTxt;
    [SerializeField] private GameObject levelPanel;
    private static int harfAlmaHakkı = 15, buttonBasmaHak;
    public GameObject[] harfKutular = new GameObject[8];
    public int NewNumber(int r)
    {
        int a = 0;


        while (a == 0)
        {
            a = Random.Range(0, r);
            if (!numbers.Contains(a))
            {
                numbers.Add(a);
            }
            else
            {
                a = 0;
            }
        }
        return a;
    }
    int GetRandom(int min, int max)
    {
        int rand = Random.Range(min, max);
        while (rand == lastNumber)
            rand = Random.Range(min, max);
        lastNumber = rand;

        return rand;
    }
    void HarfYaz()
    {
        while (i < kelimeler.Length)
        {
            //Debug.Log(kelimeler[i]);
            harf = kelimeler[i];
            harfLenght = harf.Length;
            for (int j = 0; j < harf.Length; j++)
            {
                prefab[j].transform.GetChild(0).GetComponent<Text>().text = harf[j].ToString();
                prefab[j].SetActive(true);
                StartCoroutine(DoFadeYap(harfKutular[j]));

            }
            if (truWord == kelimeler[i].ToLower())
            {
                dogruMu = true;
                truWord = "";
                isTrue = true;
                //puan += harf.Length * 100;
                //puanTxt.text = puan.ToString();
                Debug.Log("puan: " + puan);
            }
            else
            {
                isTrue = false;
            }
            break;
        }
    }
    IEnumerator DoFadeYap(GameObject kutu)
    {
        kutu.GetComponent<CanvasGroup>().DOFade(1, 1.5f);
        yield return new WaitForSeconds(0.2f);
    }
    public void HarfAl()
    {
        if (harfAlmaHakkı > 0)
        {
            if (buttonBasmaHak > 0)
            {
            A:
                audioSource.Play();
                num = GetRandom(0, harfLenght);
                if (prefab[num].transform.GetChild(0).gameObject.activeSelf == true)
                {
                    goto A;
                }
                prefab[num].transform.GetChild(0).gameObject.SetActive(true);

                puan -= 100;
                harfAlmaHakkı--;
                buttonBasmaHak--;
            }
        }
    }
    void SoruGöster()
    {
        if (i == 0)
        {
            buttonBasmaHak = 2;
            random = Random.Range(0, harfli2.Length);
            kelimeler[i] = harfli2[random].ToUpper().TrimEnd();
            sözlükAnlamı = harfli2Anlam[random];
            SözlükText.text = sözlükAnlamı;
        }
        else if (i == 1)
        {
            buttonBasmaHak = 3;
            audioSource2.Play();
            random = Random.Range(0, harfli3.Length);
            kelimeler[i] = harfli3[random].ToUpper().TrimEnd();
            sözlükAnlamı = harfli3Anlam[random];
            SözlükText.text = sözlükAnlamı;
        }
        else if (i == 2)
        {
            buttonBasmaHak = 4;
            audioSource2.Play();
            random = Random.Range(0, harfli4.Length);
            kelimeler[i] = harfli4[random].ToUpper().TrimEnd();
            sözlükAnlamı = harfli4Anlam[random];
            SözlükText.text = sözlükAnlamı;
        }
        else if (i == 3)
        {
            buttonBasmaHak = 5;
            audioSource2.Play();
            random = Random.Range(0, harfli5.Length);
            kelimeler[i] = harfli5[random].ToUpper().TrimEnd();
            sözlükAnlamı = harfli5Anlam[random];
            SözlükText.text = sözlükAnlamı;
        }
        else if (i == 4)
        {
            buttonBasmaHak = 6;
            audioSource2.Play();
            random = Random.Range(0, harfli6.Length);
            kelimeler[i] = harfli6[random].ToUpper().TrimEnd();
            sözlükAnlamı = harfli6Anlam[random];
            SözlükText.text = sözlükAnlamı;
        }
        else if (i == 5)
        {
            buttonBasmaHak = 7;
            audioSource2.Play();
            random = Random.Range(0, harfli7.Length);
            kelimeler[i] = harfli7[random].ToUpper().TrimEnd();
            sözlükAnlamı = harfli7Anlam[random];
            SözlükText.text = sözlükAnlamı;
        }
        else if (i == 6)
        {
            buttonBasmaHak = 8;
            audioSource2.Play();
            random = Random.Range(0, harfli8.Length);
            kelimeler[i] = harfli8[random].ToUpper().TrimEnd();
            sözlükAnlamı = harfli8Anlam[random];
            SözlükText.text = sözlükAnlamı;
        }
        else if (i == 7)
        {
            buttonBasmaHak = 8;
            audioSource2.Play();
            random = Random.Range(0, harfli82.Length);
            kelimeler[i] = harfli82[random].ToUpper().TrimEnd();
            sözlükAnlamı = harfli8Anlam2[random];
            SözlükText.text = sözlükAnlamı;
            levelNumber++;
        }
        else if (i == 8)
        {
            PlayerPrefs.SetInt("Level", levelNumber);
            levelText.text = levelNumber.ToString();
            levelPanel.SetActive(true);
            i = 0;
            isGameOver=true;
        }

    }
    public void İlerle()
    {
        if (i != kelimeler.Length && dogruMu == true)
        {
            i++;
            CircleKlavye();
            dogruMu = false;
            HarfleriSil();
        }
    }
    void HarfleriSil()
    {
        for (int j = 0; j < prefab.Length; j++)
        {
            prefab[j].transform.GetChild(0).gameObject.SetActive(false);
        }
    }
    void CircleKlavye()
    {
        for (int i = 0; i < st.Length; i++)
        {
            harfListe.Add(st[i]);
        }
        //Random Harfleri yazdırma
        for (int a = 0; a < 8; a++)
        {
            int x = Random.Range(0, harfListe.Count);
            c = harfListe[x];
            harfListe.Remove(c);
            circle.transform.GetChild(a).GetChild(0).GetComponent<Text>().text = c.ToString();
        }
        //Txt dosyasından random kelimeler seçme
        SoruGöster();
        /////////////////////////////////
        // Doğru harfleri yazdırma
        numbers.Clear();
        while (i < kelimeler.Length)
        {
            harf2 = kelimeler[i];
            List<int> randomSayılar = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7 };
            for (int j = 0; j < harf2.Length; j++)
            {
                //Random sayıyı bul
                int rnd;
                rnd = randomSayılar[Random.Range(0, randomSayılar.Count)];
                randomSayılar.Remove(rnd);
                circle.transform.GetChild(rnd).GetChild(0).GetComponent<Text>().text = harf2[j].ToString();
            }
            break;
        }
    }
    void WriteWord()
    {
        wordText.text = truWord;
        wordText.text = wordText.text.ToUpper();
    }
    void CreateDirectory()
    {
        Directory.CreateDirectory(Application.streamingAssetsPath + "/");
    }
    void ReadTextAsset()
    {
        harfli2 = harfli2Asset.text.Split('\n');
        RemoveSpace(harfli2);
        harfli2Anlam = harfli2AnlamAsset.text.Split('\n');
        harfli2.ToString().ToLower();

        harfli3 = harfli3Asset.text.Split('\n');
        harfli3Anlam = harfli3AnlamAsset.text.Split('\n');
        harfli3.ToString().ToLower();

        harfli4 = harfli4Asset.text.Split('\n');
        harfli4Anlam = harfli4AnlamAsset.text.Split('\n');
        harfli4.ToString().ToLower();

        harfli5 = harfli5Asset.text.Split('\n');
        harfli5Anlam = harfli5AnlamAsset.text.Split('\n');
        harfli5.ToString().ToLower();

        harfli6 = harfli6Asset.text.Split('\n');
        harfli6Anlam = harfli6AnlamAsset.text.Split('\n');
        harfli6.ToString().ToLower();

        harfli7 = harfli7Asset.text.Split('\n');
        harfli7Anlam = harfli7AnlamAsset.text.Split('\n');
        harfli7.ToString().ToLower();

        harfli8 = harfli8Asset.text.Split('\n');
        harfli8Anlam = harfli8AnlamAsset.text.Split('\n');
        harfli8.ToString().ToLower();

        harfli82 = harfli8Asset2.text.Split('\n');
        harfli8Anlam2 = harfli8AnlamAsset2.text.Split('\n');
        harfli82.ToString().ToLower();


    }
    void RemoveSpace(string[] tmp)
    {
        for (int i = 0; i < tmp.Length; i++)
        {
            if (tmp[i] == "\n" || tmp[i] == " ")
            {
                tmp[i].Remove(i);
            }
            // Debug.Log(tmp[i]);
        }
    }
    private void Awake()
    {
        ReadTextAsset();
    }
    void Start()
    {
        harfAlmaHakkı=15;
        levelNumber = PlayerPrefs.GetInt("Level", levelNumber);
        Debug.Log("Leve is : " + levelNumber);
        CircleKlavye();
    }
    void Update()
    {
        harflAlmaHakTxt.text = harfAlmaHakkı.ToString();
        HarfYaz();
        İlerle();
        WriteWord();
    }
}
