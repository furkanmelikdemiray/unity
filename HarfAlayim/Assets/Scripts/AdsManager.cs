﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;
using UnityEngine.SceneManagement;
public class AdsManager : MonoBehaviour
{
    InterstitialAd interstitial;
    void Awake()
    {
        MobileAds.Initialize(initStatus => { });
        this.RequesInterstitial();
    }
    void Update()
    {
        if (this.interstitial.IsLoaded())
        {
            if (GameManager.isGameOver == true)
            {
                this.interstitial.Show();
                GameManager.isGameOver = false;
            }
        }
    }
    public void ShowAd()
    {
        this.interstitial.Show();
    }
    void RequesInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-4138495005018017/6961931349";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        this.interstitial = new InterstitialAd(adUnitId);
        // Called when an ad request has successfully loaded.
        this.interstitial.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        this.interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        // Called when an ad is shown.
        this.interstitial.OnAdOpening += HandleOnAdOpening;
        // Called when the ad is closed.
        this.interstitial.OnAdClosed += HandleOnAdClosed;

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        this.interstitial.LoadAd(request);

    }
    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        // MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
        //                   + args.Message);
    }

    public void HandleOnAdOpening(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpening event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
    }
}