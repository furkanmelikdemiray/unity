Gezgin derviş, tasavvufta manevi üst bir rütbe
Anıt
Gece kıyafeti
Yayvan ve dolgun(yüz)
Süreli yayınları, parasını önceden ödeyerek alma işi
Şüphe, kuşku
Hızlı yapılan, çabuk, tez, ivedi
Bir işin yabancısı olan, eli işe alışmamış, bir işi beceremeyen
Görülmemiş, alışılmamış, şaşılacak veya yadırganacak şey
Huysuz, yaşlı kadın
Satranç oyununda şahı koruyan taşlardan birinin yerinden oynatılamaması durumu, içinden zor çıkılır durum
Kas
Bir kimsenin oturduğu yer, bulunak
Gereksiz, önemsiz (söz)
Olgunlaşmamış haşhaş kapsüllerine yapılan çiziklerden sızan, güçlü bir zehir olmakla birlikte içinde morfin, kodein vb. uyuşturucular bulunan madde
Aralarında aynı yerde bulunmaktan başka hiçbir ortak özellik bulunmayan kişilerden oluşan topluluk, halk
Kendisiyle yakın ilişki kurulup sevilen, sayılan kimse
Uyum, uzlaşma, ezgi
Bir toplum içinde kişilerin uymak zorunda oldukları davranış biçimleri ve kuralları, aktöre, sağtöre
Aklını gereği gibi kullanamayan, bön, budala
Ağaçtan, tahtadan yapılmış
Durumlar, hâller, vaziyetler
Dernek, kuruluş, kulüp üyelerinin belli sürelerde, belli miktarlarda ödedikleri para, ödenti
Haber toplama, yayma ve üyelerine dağıtma işiyle uğraşan kuruluş
Tehlikeli, sarp ve zor geçit
Bir çalgıda doğru ses vermesi için yapılan ayar, düzen
Saatin iki ibresinden küçüğü
Aksayan, hafifçe topallayan
Bir ülkenin insanlarına veya bir çevreye özgü söyleyiş özelliği
Güneşin batmasına yakın zamandan gecenin başlamasına kadar olan vakit
Baharat veya güzel kokular satan kimse veya dükkân
Etkin, canlı, hareketli, çalışkan, faal
Erkek oyuncu
İki veya daha çok renkli
İlgi
Bir uyarıyı, bir tehlikeyi bildirmek için verilen işaret
Dökülen tohumlarla ertesi yıl kendiliğinden çıkan tahıl, soğan vb
Rütbesi yarbay ile tuğgeneral arasında bulunan ve asıl görevi alay komutanlığı olan üstsubay
Fotoğraf, pul vb.ni dizip saklamaya yarayan bir defter türü
Bile bile en kötü, en ahlaksızca davranışlarda bulunan, aşağılık, soysuz, namert, rezil
Açık, ortada, meydanda, herkesin içinde yapılan
Hz. Ali'ye bağlı olan kimse
Bir şeyin veya bir kimsenin karşısında olma, leh karşıtı




















